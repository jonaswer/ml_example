import numpy as np 
import matplotlib.pyplot as plt 
from sklearn import svm, datasets
import sys 
import json 

def readData(filename):

    with open(filename) as f:
        
        lines = f.readlines()
        data_cleaned = []
        index = ['sepal_length_in_cm','sepal_width_in_cm','petal_length_in_cm','petal_width_in_cm','class_of_flower']

        for line_nr, line in enumerate(lines):

            try:
                line_as_list = line.split(",")
                line_as_list_cleaned = []

                sepal_length_in_cm = float(line_as_list[0])
                line_as_list_cleaned.append(sepal_length_in_cm)
                sepal_width_in_cm = float(line_as_list[1])
                line_as_list_cleaned.append(sepal_width_in_cm)
                petal_length_in_cm = float(line_as_list[2])
                line_as_list_cleaned.append(petal_length_in_cm)
                petal_width_in_cm = float(line_as_list[3])
                line_as_list_cleaned.append(petal_width_in_cm)
                class_of_flower = line_as_list[4].split("\n")[0]
                line_as_list_cleaned.append(class_of_flower)

                data_cleaned.append(line_as_list_cleaned)
            except:
                if (len(line.split(",")) == 1):
                    continue
                else:
                    raise ValueError("Datapoint is missing")

        data_cleaned_np = np.array(data_cleaned)

    return data_cleaned_np
    
def make_meshgrid(x, y, h=0.02):
    '''Create a mesh of points to plot in'''

    #Parameters
    #----------
    #x: data to base x-axis meshgrid on 
    #y: data to base y-axis meshgrid on 
    #h: stepsize for meshgrid optional 

    #Returns
    #---------

    x_min = x.min() - 1
    x_max = x.max() + 1
    y_min = y.min() - 1
    y_max = y.max() + 1 

    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    return xx, yy 

def plot_contours(ax, clf, xx, yy, **params):
    '''Plot the decision boundaries for a classifier'''

    #Parameters
    #----------
    #ax: matplotlib axes object
    #clf: a classifier
    #xx: meshgrid ndarray
    #yy: meshgrid ndarray
    #params: dictionaray of params to pass to contourf, optional 

    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = ax.contourf(xx,yy, Z, **params)

    return out 

def create_model(plot):

    # import some data wiith sklearn
    iris = datasets.load_iris()
    # Take the first two features. We could avoid this by using a two-dim dataset
    X = iris.data[:, :2]
    y = iris.target
  
    target_names = iris.target_names

    # we create an instance of SVM and fit out data. We do not scale our
    # data since we want to plot the support vectors
    C = 1.0  # SVM regularization parameter
    models = (
        svm.SVC(kernel="linear", C=C),
        svm.LinearSVC(C=C, max_iter=10000),
        svm.SVC(kernel="rbf", gamma=0.7, C=C),
        svm.SVC(kernel="poly", degree=3, gamma="auto", C=C),
    )
    models = (clf.fit(X, y) for clf in models)

    # title for the plots
    titles = (
        "SVC_with_linear_kernel",
        "LinearSVC_(linear_kernel)",
        "SVC_with_RBF_kernel",
        "SVC_with_polynomial_(degree_3)_kernel",
    )

    # Set-up 2x2 grid for plotting.
    fig, sub = plt.subplots(2, 2)
    plt.subplots_adjust(wspace=0.4, hspace=0.4)

    X0, X1 = X[:, 0], X[:, 1]
    xx, yy = make_meshgrid(X0, X1)

    if plot == "True":
    
        for clf, title, ax in zip(models, titles, sub.flatten()):
            plot_contours(ax, clf, xx, yy, cmap=plt.cm.coolwarm, alpha=0.8)
            ax.scatter(X0, X1, c=y, cmap=plt.cm.coolwarm, s=20, edgecolors="k")
            ax.set_xlim(xx.min(), xx.max())
            ax.set_ylim(yy.min(), yy.max())
            ax.set_xlabel("Sepal length")
            ax.set_ylabel("Sepal width")
            ax.set_xticks(())
            ax.set_yticks(())
            ax.set_title(title)

        plt.show()

    return models, target_names, titles

#if __name__ == "__main__":
def getLabel(features):

    with open('database.json') as f:
        database = json.load(f)
        #print(database['plot'])

    plot = database['plot']
    model_selection = database['model_selection']

    #print(plot)

    models, target_names, titles = create_model(plot)

    predictions = []

    for key, value in enumerate(titles):
        if str(value) == str(model_selection):
            chosenKey = key
        else:
            continue

    if 'chosenKey' in locals():
        pass
    else:
        chosenKey = 0
        raise ValueError("Selected Model not found")

    for model in models:
        #prediction = model.predict([[5, 2]])
        feature_1=features.split('_')[0]
        feature_2=features.split('_')[1]

        prediction = model.predict([[float(feature_1),float(feature_2)]])
        predictions.append(prediction)

    pred=int(predictions[chosenKey]) #0 = linear kernel
    
    return target_names[pred], titles[chosenKey]

if __name__ == "__main__":

    with open('database.json') as f:
        database = json.load(f)
        #print(database['plot'])

    plot = database['plot']
    model_selection = database['model_selection']

    #print(plot)

    models, target_names, titles = create_model(plot)

    predictions = []

    for key, value in enumerate(titles):
        if str(value) == str(model_selection):
            chosenKey = key
        else:
            continue

    if 'chosenKey' in locals():
        pass
    else:
        chosenKey = 0
        raise ValueError("Selected Model not found")

    for model in models:
        #prediction = model.predict([[5, 2]])
        prediction = model.predict([[float(sys.argv[1]),float(sys.argv[2])]])
        predictions.append(prediction)

    pred=int(predictions[chosenKey]) #0 = linear kernel
    print(target_names[pred])
    

  






