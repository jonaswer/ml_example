import flask 
import main 

app = flask.Flask(__name__)

@app.route('/<features>', methods=['GET'])
def home(features):

    label,model = main.getLabel(features) 
    return ("<h1>Due to the features, the model predict the input belongs to the flower:<h1>" + label + "<h1>Selected Model:<h1>" + model)

if __name__ == '__main__':
    #app.run(debug=True, host="172.17.0.2")
    app.run(debug=True, host="0.0.0.0")#local


