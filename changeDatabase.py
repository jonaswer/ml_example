import json 
import sys 

try:
    model_selection = str(sys.argv[2])
    plot = str(sys.argv[1])
    data = {"model_selection" : model_selection, "plot" : plot}
except:
    raise AssertionError("Can not update database")

with open('database.json', 'w') as f:
    json.dump(data, f)
