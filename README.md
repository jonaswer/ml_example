Instructions:

Install Environment with the Requirements file "req.txt"

Call Script with "Python main.py <feature_1> <feature_2>"

Run Server with "Python app.py"
Call Server with "http://localhost:5000/<feature_1>_<feature_2>"

This code is partially based on the Scikit Learn instructions for the Support Vector Machine Library
(https://scikit-learn.org/stable/modules/svm.html)


