# set base image (host OS)
FROM python:3.8

# set the working directory in the container
WORKDIR /

# copy the dependencies file to the working directory
COPY req.txt .

# install dependencies
RUN pip install -r req.txt

# copy the content of the local src directory to the working directory
COPY / .

# command to run on container start
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]

#EXPOSE 5000

#CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]